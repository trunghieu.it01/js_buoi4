function sapXep() {
  var soThuNhat = document.getElementById("txt-so-thu-nhat").value * 1;
  var soThuHai = document.getElementById("txt-so-thu-hai").value * 1;
  var soThuBa = document.getElementById("txt-so-thu-ba").value * 1;
  if (soThuNhat > soThuHai) {
    if (soThuHai > soThuBa) {
      document.getElementById(
        "resultEx1"
      ).innerHTML = `<p> Kết quả: ${soThuBa} < ${soThuHai} < ${soThuNhat} </p>`;
    } else {
      if (soThuNhat > soThuBa) {
        document.getElementById(
          "resultEx1"
        ).innerHTML = `<p> Kết quả: ${soThuHai} < ${soThuBa} < ${soThuNhat} </p>`;
      } else {
        document.getElementById(
          "resultEx1"
        ).innerHTML = `<p> Kết quả: ${soThuHai} < ${soThuNhat} < ${soThuBa} </p>`;
      }
    }
  } else {
    if (soThuHai > soThuBa) {
      if (soThuNhat > soThuBa) {
        document.getElementById(
          "resultEx1"
        ).innerHTML = `<p> Kết quả: ${soThuBa} < ${soThuNhat} < ${soThuHai} </p>`;
      } else {
        document.getElementById(
          "resultEx1"
        ).innerHTML = `<p> Kết quả: ${soThuNhat} < ${soThuBa} < ${soThuHai} </p>`;
      }
    } else {
      document.getElementById(
        "resultEx1"
      ).innerHTML = `<p> Kết quả: ${soThuNhat} < ${soThuHai} < ${soThuBa} </p>`;
    }
  }
}
function guiLoiChao() {
  var select = document.getElementById("txt-xinchao");
  var option = select.options[select.selectedIndex];
  console.log("option", option.value);
  var kieuChao = option.value;
  switch (kieuChao) {
    case "bo": {
      document.getElementById("resultEx2").innerHTML = `<p>Chào bố  </p>`;
      break;
    }
    case "me": {
      document.getElementById("resultEx2").innerHTML = `<p>Chào mẹ</p>`;
      break;
    }
    case "anhTrai": {
      document.getElementById("resultEx2").innerHTML = `<p>Chào anh trai</p>`;
      break;
    }
    case "emGai": {
      document.getElementById("resultEx2").innerHTML = `<p>Chào chị gái</p>`;
      break;
    }
    default:
      document.getElementById(
        "resultEx2"
      ).innerHTML = `<p>Vui lòng chọn người dùng</p>`;
  }
}

function demSoChanLe() {
  var soThuNhat = document.getElementById("txt-so-thu-nhat-ex3").value * 1;
  var soThuHai = document.getElementById("txt-so-thu-hai-ex3").value * 1;
  var soThuBa = document.getElementById("txt-so-thu-ba-ex3").value * 1;
  var soLuongSoChan = 0;
  if (soThuNhat % 2 == 0) {
    soLuongSoChan++;
  }

  if (soThuHai % 2 == 0) {
    soLuongSoChan++;
  }
  if (soThuBa % 2 == 0) {
    soLuongSoChan++;
  }
  var soLuongSoLe = 3 - soLuongSoChan;
  document.getElementById(
    "resultEx3"
  ).innerHTML = `<p> ${soLuongSoChan} số chẵn ; ${soLuongSoLe} số lẻ</p>`;
}
function doanHinhTamGiac() {
  var canhThuNhat = document.getElementById("txt-canh-thu-nhat").value * 1;
  var canhThuHai = document.getElementById("txt-canh-thu-hai").value * 1;
  var canhThuBa = document.getElementById("txt-canh-thu-ba").value * 1;
  if (canhThuNhat == canhThuHai && canhThuNhat == canhThuBa) {
    document.getElementById(
      "resultEx4"
    ).innerHTML = `<p> Kết quả: Tam giác đều </p>`;
  } else if (
    canhThuNhat == canhThuHai ||
    canhThuNhat == canhThuBa ||
    canhThuHai == canhThuBa
  ) {
    document.getElementById(
      "resultEx4"
    ).innerHTML = `<p> Kết quả: Tam giác cân </p>`;
  } else if (
    Math.pow(canhThuNhat, 2) ==
      Math.pow(canhThuHai, 2) + Math.pow(canhThuBa, 2) ||
    Math.pow(canhThuHai, 2) ==
      Math.pow(canhThuNhat, 2) + Math.pow(canhThuBa, 2) ||
    Math.pow(canhThuBa, 2) == Math.pow(canhThuNhat, 2) + Math.pow(canhThuHai, 2)
  ) {
    document.getElementById(
      "resultEx4"
    ).innerHTML = `<p> Kết quả: Tam giác vuông </p>`;
  } else
    document.getElementById(
      "resultEx4"
    ).innerHTML = `<p> Kết quả: Tam giác khác </p>`;
}
function tinhNgayHomQua() {
  var ngayNhapVao = document.getElementById("txt-ngay").value * 1;
  var thangNhapVao = document.getElementById("txt-thang").value * 1;
  var namNhapVao = document.getElementById("txt-nam").value * 1;
  var ngayQua, thangTruoc, namTruoc;
  if (
    thangNhapVao == 5 ||
    thangNhapVao == 7 ||
    thangNhapVao == 10 ||
    thangNhapVao == 12
  ) {
    if (ngayNhapVao == 1) {
      ngayQua = 30;
      thangTruoc = thangNhapVao - 1;
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p> ${ngayQua}/${thangTruoc}/${namNhapVao} </p>`;
    } else {
      ngayQua = ngayNhapVao - 1;
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p> ${ngayQua}/${thangNhapVao}/${namNhapVao} </p>`;
    }
  } else {
    if (ngayNhapVao == 1 && thangNhapVao == 3) {
      ngayQua = 28;
      thangTruoc = thangNhapVao - 1;
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p> ${ngayQua}/${thangTruoc}/${namNhapVao} </p>`;
    } else if (ngayNhapVao == 1 && thangNhapVao != 1) {
      ngayQua = 31;
      thangTruoc = thangNhapVao - 1;
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p> ${ngayQua}/${thangTruoc}/${namNhapVao} </p>`;
    } else if (ngayNhapVao == 1 && thangNhapVao == 1) {
      ngayQua = 31;
      thangTruoc = 12;
      namTruoc = namNhapVao - 1;
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p> ${ngayQua}/${thangTruoc}/${namTruoc} </p>`;
    } else if (ngayNhapVao > 28 && thangNhapVao == 2) {
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p>Tháng 2 chỉ có 28 ngày, vui lòng nhập lại.`;
    } else {
      ngayQua = ngayNhapVao - 1;
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p> ${ngayQua}/${thangNhapVao}/${namNhapVao} </p>`;
    }
  }
}
function tinhNgayMai() {
  var ngayNhapVao = document.getElementById("txt-ngay").value * 1;
  var thangNhapVao = document.getElementById("txt-thang").value * 1;
  var namNhapVao = document.getElementById("txt-nam").value * 1;
  var ngayMai, thangSau, namSau;
  if (
    thangNhapVao == 1 ||
    thangNhapVao == 3 ||
    thangNhapVao == 5 ||
    thangNhapVao == 7 ||
    thangNhapVao == 8 ||
    thangNhapVao == 10 ||
    thangNhapVao == 12
  ) {
    if (ngayNhapVao == 31 && thangNhapVao != 12) {
      ngayMai = 1;
      thangSau = thangNhapVao + 1;
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p> ${ngayMai}/${thangSau}/${namNhapVao} </p>`;
    } else if (ngayNhapVao == 31 && thangNhapVao == 12) {
      ngayMai = 1;
      thangSau = 1;
      namSau = namNhapVao + 1;
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p> ${ngayMai}/${thangSau}/${namSau} </p>`;
    } else {
      ngayMai = ngayNhapVao + 1;
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p> ${ngayMai}/${thangNhapVao}/${namNhapVao} </p>`;
    }
  } else {
    if (ngayNhapVao > 28 && thangNhapVao == 2) {
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p>Tháng 2 chỉ có 28 ngày, vui lòng nhập lại.`;
    } else if (ngayNhapVao == 30) {
      ngayMai = 1;
      thangSau = thangNhapVao + 1;
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p> ${ngayMai}/${thangSau}/${namNhapVao} </p>`;
    } else if (ngayNhapVao == 28 && thangNhapVao == 2) {
      ngayMai = 1;
      thangSau = thangNhapVao + 1;
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p> ${ngayMai}/${thangSau}/${namNhapVao} </p>`;
    } else {
      ngayMai = ngayNhapVao + 1;
      document.getElementById(
        "resultEx5"
      ).innerHTML = `<p> ${ngayMai}/${thangNhapVao}/${namNhapVao} </p>`;
    }
  }
}
function tinhNgay() {
  var thangNhapVao = document.getElementById("txt-thang-ex6").value * 1;
  var namNhapVao = document.getElementById("txt-nam-ex6").value * 1;
  if (thangNhapVao == 2 && namNhapVao % 4 == 0 && namNhapVao % 100 != 0) {
    document.getElementById(
      "resultEx6"
    ).innerHTML = `<p> Tháng ${thangNhapVao} năm ${namNhapVao} có 29 ngày </p>`;
  } else if (thangNhapVao == 2 && namNhapVao % 400 == 0) {
    document.getElementById(
      "resultEx6"
    ).innerHTML = `<p> Tháng ${thangNhapVao} năm ${namNhapVao} có 29 ngày </p>`;
  } else if (thangNhapVao == 2) {
    document.getElementById(
      "resultEx6"
    ).innerHTML = `<p> Tháng 2 năm ${namNhapVao} có 28 ngày </p>`;
  } else if (
    thangNhapVao == 4 ||
    thangNhapVao == 6 ||
    thangNhapVao == 9 ||
    thangNhapVao == 11
  ) {
    document.getElementById(
      "resultEx6"
    ).innerHTML = `<p> Tháng ${thangNhapVao} năm ${namNhapVao} có 30 ngày </p>`;
  } else if (
    thangNhapVao == 1 ||
    thangNhapVao == 3 ||
    thangNhapVao == 5 ||
    thangNhapVao == 7 ||
    thangNhapVao == 8 ||
    thangNhapVao == 10 ||
    thangNhapVao == 12
  ) {
    document.getElementById(
      "resultEx6"
    ).innerHTML = `<p> Tháng ${thangNhapVao} năm ${namNhapVao} có 31 ngày </p>`;
  } else {
    document.getElementById(
      "resultEx6"
    ).innerHTML = `<p> Tháng nhập vào không hợp lệ, vui lòng nhập lại </p>`;
  }
}
function docSo() {
  var soNhapVao = document.getElementById("txt-so-nhap-vao").value * 1;
  var hangTram = Math.floor(soNhapVao / 100);
  var hangChuc = Math.floor((soNhapVao - hangTram * 100) / 10);
  var hangDonVi = soNhapVao - hangTram * 100 - hangChuc * 10;
  var cachDocHangTram, cachDocHangDonVi, cahDocHangChuc;
  switch (hangTram) {
    case 1:
      cachDocHangTram = "một";
      break;
    case 2:
      cachDocHangTram = "hai";
      break;
    case 3:
      cachDocHangTram = "ba";
      break;
    case 4:
      cachDocHangTram = "bốn";
      break;
    case 5:
      cachDocHangTram = "năm";
      break;
    case 6:
      cachDocHangTram = "sáu";
      break;
    case 7:
      cachDocHangTram = "bảy";
      break;
    case 8:
      cachDocHangTram = "tám";
      break;
    case 9:
      cachDocHangTram = "chín";
      break;
  }
  switch (hangChuc) {
    case 1:
      cahDocHangChuc = "mười";
      break;
    case 2:
      cahDocHangChuc = "hai mươi";
      break;
    case 3:
      cahDocHangChuc = "ba mươi";
      break;
    case 4:
      cahDocHangChuc = "bốn mươi";
      break;
    case 5:
      cahDocHangChuc = "năm mươi";
      break;
    case 6:
      cahDocHangChuc = "sáu mươi";
      break;
    case 7:
      cahDocHangChuc = "bảy mươi";
      break;
    case 8:
      cahDocHangChuc = "tám mươi";
      break;
    case 9:
      cahDocHangChuc = "chín mươi";
      break;
  }
  switch (hangDonVi) {
    case 1:
      cachDocHangDonVi = "một";
      break;
    case 2:
      cachDocHangDonVi = "hai";
      break;
    case 3:
      cachDocHangDonVi = "ba";
      break;
    case 4:
      cachDocHangDonVi = "bốn";
      break;
    case 5:
      cachDocHangDonVi = "lăm";
      break;
    case 6:
      cachDocHangDonVi = "sáu";
      break;
    case 7:
      cachDocHangDonVi = "bảy";
      break;
    case 8:
      cachDocHangDonVi = "tám";
      break;
    case 9:
      cachDocHangDonVi = "chín";
      break;
  }
  if (hangDonVi == 0 && hangChuc != 0) {
    document.getElementById(
      "resultEx7"
    ).innerHTML = `<p>${cachDocHangTram} trăm ${cahDocHangChuc}</p>`;
  } else if (hangDonVi != 0 && hangChuc == 0) {
    document.getElementById(
      "resultEx7"
    ).innerHTML = `<p>${cachDocHangTram} trăm lẻ ${cachDocHangDonVi} </p> `;
  } else if (hangDonVi == 0 && hangDonVi == 0) {
    document.getElementById(
      "resultEx7"
    ).innerHTML = `<p>${cachDocHangTram} trăm </p>`;
  } else {
    document.getElementById(
      "resultEx7"
    ).innerHTML = `<p>${cachDocHangTram} trăm ${cahDocHangChuc} ${cachDocHangDonVi} </p> `;
  }
}
function timKhoangCachXaNhat() {
  var sinhVien1 = document.getElementById("txt-sinh-vien-1").value;
  var sinhVien2 = document.getElementById("txt-sinh-vien-2").value;
  var sinhVien3 = document.getElementById("txt-sinh-vien-3").value;
  var toaDoXSinhVien1 = document.getElementById("txt-toa-do-x1").value * 1;
  var toaDoXSinhVien2 = document.getElementById("txt-toa-do-x2").value * 1;
  var toaDoXSinhVien3 = document.getElementById("txt-toa-do-x3").value * 1;
  var toaDoYSinhVien1 = document.getElementById("txt-toa-do-y1").value * 1;
  var toaDoYSinhVien2 = document.getElementById("txt-toa-do-y2").value * 1;
  var toaDoYSinhVien3 = document.getElementById("txt-toa-do-y3").value * 1;
  var toaDoXTruongHoc = document.getElementById("txt-toa-do-x").value * 1;
  var toaDoYTruongHoc = document.getElementById("txt-toa-do-y").value * 1;
  var khoangCachTruongDenSinhVien1 = Math.sqrt(
    Math.pow(toaDoXTruongHoc - toaDoXSinhVien1, 2) +
      Math.pow(toaDoYTruongHoc - toaDoYSinhVien1, 2)
  );
  var khoangCachTruongDenSinhVien2 = Math.sqrt(
    Math.pow(toaDoXTruongHoc - toaDoXSinhVien2, 2) +
      Math.pow(toaDoYTruongHoc - toaDoYSinhVien2, 2)
  );
  var khoangCachTruongDenSinhVien3 = Math.sqrt(
    Math.pow(toaDoYTruongHoc - toaDoXSinhVien3, 2) +
      Math.pow(toaDoYTruongHoc - toaDoYSinhVien3, 2)
  );
  console.log("khoangCachTruongDenSinhVien1: ", khoangCachTruongDenSinhVien1);
  console.log("khoangCachTruongDenSinhVien2: ", khoangCachTruongDenSinhVien2);
  console.log("khoangCachTruongDenSinhVien3: ", khoangCachTruongDenSinhVien3);
  if (
    khoangCachTruongDenSinhVien1 > khoangCachTruongDenSinhVien2 &&
    khoangCachTruongDenSinhVien1 > khoangCachTruongDenSinhVien3
  ) {
    document.getElementById(
      "resultEx8"
    ).innerHTML = `<p>Sinh viên xa trường nhất: ${sinhVien1} </p> `;
  } else if (
    khoangCachTruongDenSinhVien2 > khoangCachTruongDenSinhVien1 &&
    khoangCachTruongDenSinhVien2 > khoangCachTruongDenSinhVien3
  ) {
    document.getElementById(
      "resultEx8"
    ).innerHTML = `<p>Sinh viên xa trường nhất: ${sinhVien2} </p> `;
  } else {
    document.getElementById(
      "resultEx8"
    ).innerHTML = `<p>Sinh viên xa trường nhất: ${sinhVien3} </p> `;
  }
}
